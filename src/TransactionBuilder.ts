import {ECPair, ECSignature, Hash, Input, Output} from './Types'
import {Transaction} from './Transaction'

interface InputWithKey {
    input:Input
    pair?:ECPair
}

export class TransactionBuilder {
    inputs:Array<InputWithKey>
    outputs:Array<Output>

    constructor() {
        this.inputs = []
        this.outputs = []
    }

    addInput(txHash:Hash, index:number, pair:ECPair) {
        this.inputs.push({
            pair:pair,
            input: new Input({
                    kind:'outpoint',
                    txHash:txHash,
                    index:index
                })
        })
    }
}