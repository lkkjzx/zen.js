import {Buffer} from 'buffer'

export function getSizeOfVarInt(value:number) {
    let size = 0
    while(true) {
        size++
        if (value <= 0x7F)
            break;
        value = (value >> 7) - 1
    }
    return size
}

export function writeVarInt (value:number, buffer:Buffer, offset:number) {
    const tmp = new Uint8Array(5)
    let len=0;

    while(true){
        tmp[len] = (value & 0x7F) | (len !== 0 ? 0x80 : 0x00)
        if (value <= 0x7F)
            break

        value = (value >> 7) - 1
        len++
    }

    tmp.reverse()
    Buffer.from(tmp).copy(buffer,offset,0,len+1)

    return offset+len+1
}

export function readVarInt (buffer:Buffer, offset: number) {
    let value = 0
    while(true) {
        let chData = buffer.readUInt8(offset)
        offset++

        value = (value << 7) | (chData & 0x7F)
        if ((chData & 0x80) !== 0)
            value++
        else
            return {value, offset}
    }
}

export function getSizeOfAmount(amount:number) {
    // NATHANTODO

    return 1
}

export function writeAmount(amount:number,buffer:Buffer,offset:number) {
    // NATHANTODO

    buffer.writeUInt8(1)

    return offset+1
}

export function readAmount(buffer:Buffer,offset:number) {
    // NATHANTODO

    return {amount:1,offset:offset+1}
}