import {Buffer} from 'buffer'
import {getSizeOfVarInt,writeVarInt,readVarInt,getSizeOfAmount,readAmount,writeAmount} from './Serialization'
import {ECSignature,ECPair} from 'bitcoinjs-lib'

export type ECSignature = ECSignature

export type ECPair = ECPair

export class Hash {
    bytes:Buffer

    constructor(public hash:string) {
        this.bytes = Buffer.from(this.hash,'hex')
    }

    getSize() {
        return 32
    }

    write(buffer:Buffer,offset:number) {
        this.bytes.copy(buffer,offset)

        return offset+32
    }

    static read(buffer:Buffer,offset:number) {
        const bytes = Buffer.alloc(32)
        buffer.copy(bytes,0, offset)

        const hash = new Hash(bytes.toString('hex'))

        return {hash,offset:offset+32}
    }
}

export class Asset {
    version:number
    assetType:Buffer
    subType:Buffer

    constructor(public asset:string) {
        if (asset === '00') {
            this.version = 0
            this.assetType = Buffer.alloc(32)
            this.assetType.fill(0)
            this.subType = Buffer.alloc(32)
            this.subType.fill(0)
        }
        else {
            const bytes = Buffer.from(asset, 'hex')

            this.version = bytes.readInt32BE(0)
            this.assetType = Buffer.alloc(32)
            bytes.copy(this.assetType,0,4)

            if (bytes.length === 36) {
                this.subType = Buffer.alloc(32)
                this.subType.fill(0)
            }
            else if (bytes.length == 68) {
                this.subType = Buffer.alloc(32)
                bytes.copy(this.subType,0,36)
            }
            else {
                throw 'invalid asset'
            }
        }
    }

    getSize() {
        //NATHANTODO
        return 1
    }

    write(buffer:Buffer, offset:number) {
        //NATHANTODO: serialize stuff to byte array

        offset = buffer.writeUInt8(0, offset)

        return offset
    }

    static read(buffer:Buffer, offset:number) {
        // NATHANTOOD: create and return the asset + advance and return the offset

        return {asset:new Asset('00'), offset:offset + 1}
    }
}

export class ContractId {
    version:number
    cHash:Buffer

    constructor(public contractId:string) {
        let bytes = Buffer.from(contractId,'hex')

        if (bytes.length !== 36) {
            throw 'invalid contractId'
        } else {
            this.version = bytes.readUInt32BE(0)
            this.cHash = Buffer.alloc(32)
            bytes.copy(this.cHash,0,4)
        }
    }

    getSize() {
        return getSizeOfVarInt(this.version) + 32
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(this.version, buffer, offset)

        this.cHash.copy(buffer, offset)

        return offset+32
    }

    static read(buffer:Buffer, offset:number) {
        const {value:version,offset:offset2} = readVarInt(buffer,offset)

        const bytes = Buffer.alloc(36)
        bytes.writeUInt32BE(version,0)
        buffer.copy(bytes,4,offset2)

        return {contractId:new ContractId(buffer.toString('hex')), offset: offset2+32}
    }
}

export class Spend {
    constructor(public asset:Asset,public amount:number) {

    }

    getSize() {
        return this.asset.getSize() + getSizeOfAmount(this.amount)
    }

    write(buffer:Buffer, offset:number) {
        offset = this.asset.write(buffer,offset)
        return writeAmount(this.amount,buffer,offset)
    }

    static read(buffer:Buffer, offset:number) {
        const {asset, offset:offset2} = Asset.read(buffer,offset)
        const {amount,offset:offset3} = readAmount(buffer,offset2)

        return {spend:new Spend(asset,amount),offset:offset3}
    }
}

// Locks
class Lock {
    getSize() : number {
        throw "please implement method"
    }

    write(buffer:Buffer, offset:number) : number {
        throw "please implement method"
    }

    static read(buffer:Buffer, offset:number) {
        const {value:identifier,offset:offset2} = readVarInt(buffer,offset)

        switch (identifier) {
            case 1:
                return PKLock.read(buffer,offset2)
            case 4:
                return FeeLock.read(buffer,offset2)
            default:
                const {value:size,offset:offset3} = readVarInt(buffer,offset2)
                const data = Buffer.alloc(size)
                buffer.copy(data, 0,offset3)

                return {lock:new HighVLock(identifier, data), offset:offset3+size}
        }
    }
}
export class PKLock extends Lock {
    constructor(public pkHash:Hash) {
        super()
    }

    getSize() {
        return getSizeOfVarInt(1) + getSizeOfVarInt(32) + 32
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(1,buffer,offset)
        offset = writeVarInt(32,buffer,offset)
        return this.pkHash.write(buffer, offset)
    }

    static read(buffer:Buffer, offset:number) {
        const {value:size,offset:offset2} = readVarInt(buffer,offset)
        if (size !== 32) throw "invalid pkLock size"

        const {hash,offset:offset3} = Hash.read(buffer,offset2)

        return {lock:new PKLock(hash), offset:offset3}
    }
}

export class FeeLock extends Lock {

    getSize() {
        return getSizeOfVarInt(4) + getSizeOfVarInt(0)
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(4,buffer,offset)
        offset = writeVarInt(0,buffer,offset)

        return offset
    }

    static read(buffer:Buffer, offset:number) {
        const {value:size,offset:offset2} = readVarInt(buffer,offset)
        if (size !== 0) throw "invalid feelock size"

        return {lock:new FeeLock(),offset:offset2}
    }
}

export class HighVLock extends Lock {
    constructor(public identifier:number, public data:Buffer){
        super()
    }

    getSize() {
        return getSizeOfVarInt(this.identifier) + getSizeOfVarInt(this.data.length) + this.data.length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(this.identifier,buffer,offset)
        offset = writeVarInt(this.data.length,buffer,offset)

        this.data.copy(buffer,offset)

        return offset
    }
}

// Witnesses

export class Witness {
    getSize() :number {
        throw "please implement method"
    }

    write(buffer:Buffer, offset:number) : number {
        throw "please implement method"
    }

    static read(buffer:Buffer, offset:number) {
        const {value:identifier,offset:offset2} = readVarInt(buffer,offset)

        switch (identifier) {
            case 1:
                return PKWitness.read(buffer,offset2)
            default:
                const {value:size,offset:offset3} = readVarInt(buffer,offset2)
                const data = Buffer.alloc(size)
                buffer.copy(data, 0,offset3)

                return {witness:new HighVWitness(identifier, data), offset:offset3+size}

        }
    }
}
export class PKWitness extends Witness {
    constructor(public publicKey:Buffer, public signature:Buffer) {
        super()
    }

    getSize() {
        let length = this.publicKey.length + this.signature.length

        return getSizeOfVarInt(1) + getSizeOfVarInt(length) + length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(1,buffer,offset)
        offset = writeVarInt(this.publicKey.length + this.signature.length, buffer,offset)

        offset = offset + this.publicKey.copy(buffer,offset)
        return offset + this.signature.copy(buffer,offset)
    }

    static read(buffer:Buffer, offset:number) {
        let {value:size,offset:offset2} = readVarInt(buffer,offset)

        if (size !== 33 + 64 ) throw 'invalid pkwitness size'

        const publicKey = Buffer.alloc(33)
        const signature = Buffer.alloc(64)

        offset2 = offset2 + buffer.copy(publicKey,0,offset2)
        offset2 = offset2 + buffer.copy(signature,0,offset2)

        return {witness:new PKWitness(publicKey,signature),offset:offset2}
    }
}

export class HighVWitness extends Witness {
    constructor(public identifier:number, public data:Buffer) {
        super()
    }

    getSize() {
        let length = this.data.length

        return getSizeOfVarInt(1) + getSizeOfVarInt(length) + length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(1,buffer,offset)
        offset = writeVarInt(this.data.length, buffer, offset)

        return offset + this.data.copy(buffer,offset)
    }
}

// Input

export interface Outpoint {
    kind:'outpoint'
    txHash:Hash,
    index:number
}

export interface Mint {
    kind:'mint'
    spend:Spend
}

export class Input {
    constructor(public input: Outpoint | Mint) {

    }

    getSize() {
        switch (this.input.kind) {
            case 'outpoint':
                return 1 + getSizeOfVarInt(this.input.index) + this.input.txHash.getSize()
            case 'mint':
                return 1 + this.input.spend.getSize()
        }
    }

    write(buffer:Buffer, offset:number) {
        switch (this.input.kind) {
            case 'outpoint':
                offset=buffer.writeUInt8(1,offset)
                offset=this.input.txHash.write(buffer,offset)
                return writeVarInt(this.input.index, buffer, offset)
            case 'mint':
                offset=buffer.writeUInt8(2,offset)
                return this.input.spend.write(buffer,offset)
        }
    }

    static read(buffer:Buffer, offset:number) {
        const identifier = buffer.readUInt8(offset)
        offset++

        switch (identifier) {
            case 1:
                const {hash:txHash,offset:offset2} = Hash.read(buffer,offset)
                const {value:index,offset:offset3} = readVarInt(buffer, offset2)

                return {
                    input:new Input({
                        kind:'outpoint',
                        txHash:txHash,
                        index:index
                    }), offset:offset3}
            case 2:
                const {spend,offset:offset4} = Spend.read(buffer,offset)

                return {
                    input:new Input({
                        kind:'mint',
                        spend:spend
                    }), offset:offset4}
        }
    }
}

// Output

export class Output {
    constructor (public lock:Lock,public spend:Spend) {

    }

    getSize() {
        return this.lock.getSize() + this.spend.getSize()
    }

    write(buffer:Buffer,offset:number) {
        offset =this.lock.write(buffer,offset)
        return this.spend.write(buffer,offset)
    }

    static read(buffer:Buffer,offset:number) {
        const {lock,offset:offset2} = Lock.read(buffer,offset)
        const {spend,offset:offset3} = Spend.read(buffer,offset2)

        return {output:new Output(lock,spend),offset:offset3}
    }
}

// Contracts

export class Contract {
    constructor(public version:number, public data:Buffer) {
    }

    getSize() {
        return getSizeOfVarInt(this.version) + getSizeOfVarInt(this.data.length) + this.data.length
    }

    write(buffer:Buffer,offset:number) {
        offset = writeVarInt(this.version,buffer,offset)
        offset = writeVarInt(this.data.length, buffer,offset)

        return offset + this.data.copy(buffer, offset)
    }

    static read(buffer:Buffer,offset:number) {
        const {value:version,offset:offset2} = readVarInt(buffer, offset)
        const {value:size, offset:offset3} = readVarInt(buffer, offset2)

        const data = Buffer.alloc(size)

        const offset4 = offset3 + buffer.copy(data, 0, offset2)

        return {contract: new Contract(version,data),offset:offset4 }
    }
}

